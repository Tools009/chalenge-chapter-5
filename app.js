const express = require('express')
const app = express()
const PORT = 3000
const path = require('path')
const controller = require('./controller/maincontrol')
const router = require('./routes/login')
const { route } = require('./routes/login')
const middleware = require('./middleware/middleware.js')


app.use(express.json())

app.set("view engine", "ejs")


const viewPath = path.join(__dirname,'view')

app.set("views", viewPath)

app.get('/', (req, res) => {
    res.render('project')
} )

app.use('/', router)

app.use(express.static(path.join(__dirname, 'public')));


app.use('/javascript', express.static(path.join(__dirname, 'click')));

app.listen(PORT, () => console.log(`http://localhost:${PORT}`))

app.use(middleware)

module.import = router
module.import = controller
module.import = middleware


module.exports = app